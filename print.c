///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file print.c
//
// This function is used to print the counted time with a delay
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_08_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

#include "print.h"

void printCount(struct tm temp){
   printf("Years: %d Days: %d Hours: %d Minutes: %d Seconds: %d\n",temp.tm_year, temp.tm_yday, temp.tm_hour, temp.tm_min, temp.tm_sec);
   sleep(1);
}

