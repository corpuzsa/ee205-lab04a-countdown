///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file: diff.c
//
// returns the difference between current and reference date
// 
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tm timeDiff(struct tm a, struct tm b){
   struct tm temp;

   temp.tm_year = abs(a.tm_year - b.tm_year);
   temp.tm_yday = abs(daysinYear(a) - daysinYear(b));
   temp.tm_hour = abs(a.tm_hour - b.tm_hour);
   temp.tm_min = abs(a.tm_min - b.tm_min);
   temp.tm_sec = abs(a.tm_sec - b.tm_sec);
   
   return temp;
}

int daysinYear(struct tm temp){
   int diy = temp.tm_mday;
   switch(temp.tm_mon){
      case 1:
         return diy += 31;
         break;
      case 2:
         return diy += 31+28;
         break;
      case 3:
         return diy += 31+28+31;
         break;
      case 4:
         return diy += 31+28+31+30;
         break;
      case 5:
         return diy += 31+28+31+30+31;
      case 6:
         return diy += 31+28+31+30+31+30;
         break;
      case 7:
         return diy += 31+28+31+30+31+30+31;
         break;
      case 8:
         return diy += 31+28+31+30+31+30+31+31;
         break;
      case 9:
         return diy += 31+28+31+30+31+30+31+31+30;
         break;
      case 10:
         return diy += 31+28+31+30+31+30+31+31+30+31;
         break;
      case 11:
         return diy += 31+28+31+30+31+30+31+31+30+31+30;
         break;
      default:
         return diy;
         break;
   }
}

