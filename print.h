///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file print.h
//
// header file for print.c
//
// @author Shaun Corpuz  <corpuzsa@hawaii.edu>
// @date   02_08_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <time.h>

void printCount(struct tm temp);

