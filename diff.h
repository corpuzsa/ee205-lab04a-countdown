///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file diff.h
//
// header file for diff.c
//
// 
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_02_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

struct tm timeDiff(struct tm a, struct tm b);

int daysinYear(struct tm temp);

