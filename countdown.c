///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// Usage:  countdown
//
// Result:
//   Counts down (or towards) a significant date
//
// Example:
// Reference time:  Tue Jan 21 04:26:07 PM HST 2014
//
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 57 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 58 
// Years: 7  Days: 12  Hours: 6  Minutes: 20  Seconds: 59 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 0 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 1 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 2 
// Years: 7  Days: 12  Hours: 6  Minutes: 21  Seconds: 3 
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_09_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "diff.h"
#include "counter.h"
#include "print.h"

struct tm makeRef();

int main(int argc, char* argv[]) {

   char buff[80];

   struct tm ref = makeRef();
   
   // Print the reference date selected
   strftime(buff, sizeof(ref), "%a %b %e %I:%M:%S %p %Z %Y", &ref);
   printf("Reference time: %s\n", buff);

   // Start the counter
   countCheck(ref);

   return 0;
}

// Set the reference date
struct tm makeRef(){

   struct tm ref;

   ref.tm_year = 2014 - 1900;
   ref.tm_mon = 1 - 1;
   ref.tm_mday = 21;
   ref.tm_wday = 2;
   ref.tm_hour = 16;
   ref.tm_min = 26;
   ref.tm_sec = 7;

   return ref;

}
