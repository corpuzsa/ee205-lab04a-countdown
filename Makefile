###############################################################################
# University of Hawaii, College of Engineering
# EE 205  - Object Oriented Programming
# Lab 04a - Countdown
#
# @file    Makefile
# @version 1.0
#
# @author Shaun Corpuz <corpuzsa@hawaii.edu>
# @brief  Lab 04a - Countdown - EE 205 - Spr 2021
# @date   02_02_2021
###############################################################################

all: countdown

countdown: countdown.c diff.c counter.c print.c
	gcc -o countdown countdown.c diff.c counter.c print.c

diff.o: diff.c diff.h
	gcc -c diff.c

counter.o: counter.c counter.h
	gcc -c counter.c

print.o: print.c print.h
	gcc -c print.c

clean:
	rm -f *.o countdown
