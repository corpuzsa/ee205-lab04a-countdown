///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file  counter.h
//
// Header file for counter.c
//
// 
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_09_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "diff.h"
#include "print.h"

void countCheck(struct tm ref);

void cUp(struct tm ref);

void cDown(struct tm ref);
