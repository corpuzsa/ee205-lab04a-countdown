///////////////////////////////////////////////////////////////////////////////
// University of Hawaii, College of Engineering
// EE 205 - Object Oriented Programming
// Lab 04a - Countdown
//
// @file counter.c
//
// Counts up or down based on reference time given.
// Will print the counted time and delayed by 1 second
// 
//
// @author Shaun Corpuz <corpuzsa@hawaii.edu>
// @date   02_09_2021
///////////////////////////////////////////////////////////////////////////////

#include <stdio.h>
#include <stdlib.h>
#include <time.h>

#include "diff.h"
#include "print.h"

void cUp(struct tm ref);
void cDown(struct tm ref);

void countCheck(struct tm ref){
   double diff = 0;

   time_t rawtime = time(0);

   diff = difftime(time(&rawtime),mktime(&ref));

   if(diff < 0){
      cUp(ref);
   }
   
   else{
      cDown(ref);
   }
}

void cUp(struct tm ref){
    struct tm counter;

   // Get current time
   struct tm now;
   time_t rawtime = time(0);
   now = *localtime(&rawtime);

   counter = timeDiff(ref, now);

   while(1){
       // Loop infinitely until user termination
       // Also update counter in this loop and print
       // where we are at
      for(;counter.tm_year >= -127;counter.tm_year--){
         for(;counter.tm_yday >= 0; counter.tm_yday--){
            for(;counter.tm_hour >= 0; counter.tm_hour--){
               for(;counter.tm_min >= 0; counter.tm_min--){
                  for(;counter.tm_sec >= 0; counter.tm_sec--){
                     printCount(counter);
                  }
                  counter.tm_sec = 59;
               }
               counter.tm_min = 59;
            }
            counter.tm_hour = 23;
          }
         counter.tm_yday = 364;
         sleep(1);
      }
   }
}

void cDown(struct tm ref){
   struct tm counter;

   // Get the current time
   struct tm now;
   time_t rawtime = time(0);
   now = *localtime(&rawtime);

   counter = timeDiff(now,ref);
   
   // Adjust the counter
   if((now.tm_mon - ref.tm_mon) < 0){
   counter.tm_year -= 1;
   counter.tm_yday = 365 - counter.tm_yday;
   counter.tm_hour = 23 - counter.tm_hour;
   counter.tm_min = 60 - counter.tm_min;
   }

   // Loop infinitely until user termination
   // Also update counter in this loop and print
   // where we are at
   while(1){
      for(;counter.tm_year < 128;counter.tm_year++){
         for(;counter.tm_yday < 365; counter.tm_yday++){
            for(;counter.tm_hour < 24; counter.tm_hour++){
               for(;counter.tm_min < 60; counter.tm_min++){
                  for(;counter.tm_sec < 60; counter.tm_sec++){
                     printCount(counter);
                  }
                  counter.tm_sec = 0;
               }
               counter.tm_min = 0;
            }
            counter.tm_hour = 0;
         }
         counter.tm_yday = 0;
         sleep(1);
      }
   }
}
